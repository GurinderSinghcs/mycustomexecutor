import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.Queue;

public class Worker {
	public int endThreads = 0;
	public WorkerThread[] workerThreadArray;
	public Queue<String> queue = new LinkedList<String>();
	public Worker(int threadPool) {
		workerThreadArray = new WorkerThread[threadPool];
		for(int i=0;i<threadPool;i++) {
			workerThreadArray[i] = new WorkerThread();
			workerThreadArray[i].threadNumber = i;
			workerThreadArray[i].start();
		}
	}
	
	public void addWork(String path) {
		synchronized (queue) {
			queue.add(path);
			queue.notifyAll();
		}
		
	}
	
	
	
	
	//thread class 
	public class WorkerThread extends Thread {
		int threadNumber;
		
	    public void run(){
	       while(true) {
	    	   String path = null;
	    	   synchronized (queue) {
	    		   if(queue.size()!=0) {
		    		   path = queue.remove();
		    		   System.out.println(path + ":::Threadnumber "+threadNumber );
		    		   try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    	   }
	    	   }
	    	   if(path!=null) {
	    		   
	    	   }else if(queue.size()==0 && endThreads==0){
	    		   try {
	    			   synchronized (queue) {
	    				   if(endThreads==1) {
	    					   break;
	    				   }
	    				   queue.wait();
	    			   }
					
				} catch (InterruptedException e) {
					
				}
	    	   }else if(endThreads==1) {
	    		   break;
	    	   }
	       }
	    }
	  }
}
